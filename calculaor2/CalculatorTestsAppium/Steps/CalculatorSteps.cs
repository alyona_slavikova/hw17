﻿using Appium.POM;
using NUnit.Framework;
using OpenQA.Selenium.Appium;
using OpenQA.Selenium.Appium.Android;
using System;
using TechTalk.SpecFlow;

namespace Appium.Steps
{
    [Binding]
    public class CalculatorSteps
    {
        public static AndroidDriver<AndroidElement> driver;
        public MainScreen mainScreen;


        [BeforeScenario]
        public void CreateDriver()
        {
            AppiumOptions caps = new AppiumOptions();

            caps.AddAdditionalCapability("app", @"C:\\Users\\123\\Documents\\folder\\calculaor2\\calculaor\\bin\\Release\\com.companyname.calculaor.apk");
            caps.AddAdditionalCapability("deviceName", "Pixel 2");
            caps.AddAdditionalCapability("automationName", "UIAutomator2");
            caps.AddAdditionalCapability("platformVersion", "9.0");
            caps.AddAdditionalCapability("platformName", "Android");
            caps.AddAdditionalCapability("build", "CSharp Android_Sim");
            caps.AddAdditionalCapability("name", "second_test");

            driver = new AndroidDriver<AndroidElement>(
                    new Uri("http://localhost:4723/wd/hub"), caps);
            mainScreen = new MainScreen(driver);
        }

        [Given(@"I press first number is (.*)")]
        public void GivenIPressFirstNumberIs(int p0)
        {
            mainScreen.GetNumber(p0);

        }

        [Given(@"I press plus button")]
        public void GivenIPressPlusButton()
        {
            mainScreen.ClickPlusBtn();
        }

        [Given(@"I press second number is (.*)")]
        public void GivenIPressSecondNumberIs(int p0)
        {
            mainScreen.GetNumber(p0);
        }

        [When(@"I press equal")]
        public void WhenIPressEqual()
        {
            mainScreen.ClickEqualBtn();

        }

        [Then(@"the result must be  (.*)")]
        public void ThenTheResultMustBe(double p0)
        {
            string actual = mainScreen.Gettext();
            double result = Convert.ToDouble(actual);
            Assert.AreEqual(p0, result);


        }
        [Given(@"I press substract button")]
        public void GivenIPressSubstractButton()
        {
            mainScreen.ClickMinusBtn();
        }

        [Given(@"I press multiply button")]
        public void GivenIPressMultiplyButton()
        {
            mainScreen.ClickMultiplyBtn();
        }

        [Given(@"I press divide button")]
        public void GivenIPressDivideButton()
        {
            mainScreen.ClickDivideBtn();
        }

        [Given(@"I press button five")]
        public void GivenIPressButtonFive()
        {
            mainScreen.ClickFiveBtn();
        }

        [Given(@"I press button nine")]
        public void GivenIPressButtonNine()
        {
            mainScreen.ClickNineBtn();
        }

        [Given(@"I press delete button")]
        public void GivenIPressDeleteButton()
        {
            mainScreen.ClickDeleteBtn();
        }

        [Given(@"I press eight button")]
        public void GivenIPressEightButton()
        {
            mainScreen.ClickEightBtn();
        }

        [Then(@"the result is thirteen")]
        public void ThenTheResultIsThirteen()
        {
            string actual = mainScreen.Gettext();
            Assert.AreEqual("13", actual);
        }
        [AfterScenario]

        public void CloseDriver()
        {
            driver.Quit();
        }
    }
}                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                           
