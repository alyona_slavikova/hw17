﻿using NUnit.Framework;
using OpenQA.Selenium.Appium;
using OpenQA.Selenium.Appium.Android;
using System;
using TechTalk.SpecFlow;
using BrowserStack.POM;

namespace BrowserStack.Steps
{
    [Binding]
    public class CalculatorSteps
    {
        AndroidDriver<AndroidElement> driver;
        public MainScreen mainScreen;
        //public Helper helper;
        [BeforeScenario]

        public void Driver()
        {
            AppiumOptions caps = new AppiumOptions();

            // Set your BrowserStack access credentials
            caps.AddAdditionalCapability("browserstack.user", "bsuser_sEM61O");
            caps.AddAdditionalCapability("browserstack.key", "C7B7xopRoW46UBejqdtz");

            // Set URL of the application under test
            caps.AddAdditionalCapability("app", "bs://ee23bdc723e8e0a9e24ebeb173cc29ac467816e4");

            // Specify device and os_version
            caps.AddAdditionalCapability("device", "Google Pixel 3");
            caps.AddAdditionalCapability("os_version", "9.0");

            // Specify the platform name
            caps.PlatformName = "Android";

            // Set other BrowserStack capabilities
            caps.AddAdditionalCapability("project", "First CSharp project");
            caps.AddAdditionalCapability("build", "CSharp Android");
            caps.AddAdditionalCapability("name", "first_test");


            // Initialize the remote Webdriver using BrowserStack remote URL
            // and desired capabilities defined above
            driver = new AndroidDriver<AndroidElement>(
                     new Uri("http://hub-cloud.browserstack.com/wd/hub"), caps);
            mainScreen = new MainScreen(driver);
        }


        [Given(@"I press first number is (.*)")]
        public void GivenIPressFirstNumberIs(int p0)
        {
            mainScreen.GetNumber(p0);

        }

        [Given(@"I press plus button")]
        public void GivenIPressPlusButton()
        {
            mainScreen.ClickPlusBtn();
        }

        [Given(@"I press second number is (.*)")]
        public void GivenIPressSecondNumberIs(int p0)
        {
            mainScreen.GetNumber(p0);
        }

        [When(@"I press equal")]
        public void WhenIPressEqual()
        {
            mainScreen.ClickEqualBtn();

        }

        [Then(@"the result must be  (.*)")]
        public void ThenTheResultMustBe(double p0)
        {
            string actual = mainScreen.Gettext();
            double result = Convert.ToDouble(actual);
            Assert.AreEqual(p0, result);


        }
        [Given(@"I press substract button")]
        public void GivenIPressSubstractButton()
        {
            mainScreen.ClickMinusBtn();
        }

        [Given(@"I press multiply button")]
        public void GivenIPressMultiplyButton()
        {
            mainScreen.ClickMultiplyBtn();
        }

        [Given(@"I press divide button")]
        public void GivenIPressDivideButton()
        {
            mainScreen.ClickDivideBtn();
        }

        [Given(@"I press button five")]
        public void GivenIPressButtonFive()
        {
            mainScreen.ClickFiveBtn();
        }

        [Given(@"I press button nine")]
        public void GivenIPressButtonNine()
        {
            mainScreen.ClickNineBtn();
        }

        [Given(@"I press delete button")]
        public void GivenIPressDeleteButton()
        {
            mainScreen.ClickDeleteBtn();
        }

        [Given(@"I press eight button")]
        public void GivenIPressEightButton()
        {
            mainScreen.ClickEightBtn();
        }

        [Then(@"the result is thirteen")]
        public void ThenTheResultIsThirteen()
        {
            string actual = mainScreen.Gettext();
            Assert.AreEqual("13", actual);
        }
        [AfterScenario]

        public void CloseDriver()
        {
            driver.Quit();
        }

    }
}

