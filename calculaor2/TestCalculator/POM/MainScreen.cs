﻿using System;
using OpenQA.Selenium.Appium;
using OpenQA.Selenium.Appium.Android;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenQA.Selenium;
using NUnit.Framework;

namespace BrowserStack.POM
{
    public class MainScreen
    {
        public AndroidDriver<AndroidElement> _driver;

        public MainScreen(AndroidDriver<AndroidElement> driver)
        {
            this._driver = driver;
        }

        public By oneButton = By.XPath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.view.ViewGroup/android.widget.Button[11]");
        public By twoButton = By.XPath("/ hierarchy / android.widget.FrameLayout / android.widget.LinearLayout / android.widget.FrameLayout / android.widget.LinearLayout / android.view.ViewGroup / android.widget.Button[12]");
        public By threeButton = By.XPath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.view.ViewGroup/android.widget.Button[13]");
        public By fourButton = By.XPath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.view.ViewGroup/android.widget.Button[7]");
        public By fiveButton = By.XPath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.view.ViewGroup/android.widget.Button[8]");
        public By sixButton = By.XPath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.view.ViewGroup/android.widget.Button[9]");
        public By sevenButton = By.XPath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.view.ViewGroup/android.widget.Button[3]");
        public By eightButton = By.XPath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.view.ViewGroup/android.widget.Button[4]");
        public By nineButton = By.XPath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.view.ViewGroup/android.widget.Button[5]");
        public By plusButton = By.XPath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.view.ViewGroup/android.widget.Button[14]");
        public By minusButton = By.XPath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.view.ViewGroup/android.widget.Button[10]");
        public By equalButton = By.XPath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.view.ViewGroup/android.widget.Button[17]");
        public By dotButton = By.XPath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.view.ViewGroup/android.widget.Button[15]");
        public By multiplyButton = By.XPath("hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.view.ViewGroup/android.widget.Button[6]");
        public By zeroButton = By.XPath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.view.ViewGroup/android.widget.Button[16]");
        public By divideButton = By.XPath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.view.ViewGroup/android.widget.Button[2]");
        public By deleteButton = By.XPath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.view.ViewGroup/android.widget.Button[1]");
        public By textField = By.XPath(" /hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.HorizontalScrollView/android.widget.TextView");
        public MainScreen ClickOneBtn()
        {
            _driver.FindElement(oneButton).Click();
            return this;
        }
        public MainScreen ClickTwoBtn()
        {
            _driver.FindElement(twoButton).Click();
            return this;
        }
        public MainScreen ClickThreeBtn()
        {
            _driver.FindElement(threeButton).Click();
            return this;
        }
        public MainScreen ClickFourBtn()
        {
            _driver.FindElement(fourButton).Click();
            return this;
        }
        public MainScreen ClickFiveBtn()
        {
            _driver.FindElement(fiveButton).Click();
            return this;
        }
        public MainScreen ClickSixBtn()
        {
            _driver.FindElement(sixButton).Click();
            return this;
        }
        public MainScreen ClickSevenBtn()
        {
            _driver.FindElement(sevenButton).Click();
            return this;
        }
        public MainScreen ClickEightBtn()
        {
            _driver.FindElement(eightButton).Click();
            return this;
        }
        public MainScreen ClickNineBtn()
        {
            _driver.FindElement(nineButton).Click();
            return this;
        }
        public MainScreen ClickPlusBtn()
        {
            _driver.FindElement(plusButton).Click();
            return this;
        }
        public MainScreen ClickMinusBtn()
        {
            _driver.FindElement(minusButton).Click();
            return this;
        }
        public MainScreen ClickEqualBtn()
        {
            _driver.FindElement(equalButton).Click();
            return this;
        }
        public MainScreen ClickDotBtn()
        {
            _driver.FindElement(dotButton).Click();
            return this;
        }
        public MainScreen ClickMultiplyBtn()
        {
            _driver.FindElement(multiplyButton).Click();
            return this;
        }
        public MainScreen ClickZeroBtn()
        {
            _driver.FindElement(zeroButton).Click();
            return this;
        }
        public MainScreen ClickDivideBtn()
        {
            _driver.FindElement(divideButton).Click();
            return this;
        }
        public MainScreen ClickDeleteBtn()
        {
            _driver.FindElement(deleteButton).Click();
            return this;
        }

        public AndroidElement FindElementn()
        {
            return _driver.FindElement(textField);

        }



        public string Gettext()
        {

            return FindElementn().Text;
        }


        public void GetNumber(int p0)
        {

            switch (p0)
            {
                case 1:
                    ClickOneBtn();
                    break;
                case 2:
                    ClickTwoBtn();
                    break;
                case 3:
                    ClickThreeBtn();
                    break;
                case 4:
                    ClickFourBtn();
                    break;
                case 5:
                    ClickFiveBtn();
                    break;
                case 6:
                    ClickSixBtn();
                    break;
                case 7:
                    ClickSevenBtn();
                    break;
                case 8:
                    ClickEightBtn();
                    break;
                case 9:
                    ClickNineBtn();
                    break;
                case 0:
                    ClickZeroBtn();
                    break;
                default:
                    break;

            }


        }

    }
}


